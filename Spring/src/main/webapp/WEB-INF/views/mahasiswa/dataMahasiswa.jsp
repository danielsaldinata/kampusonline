<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<p class="navbar-text navbar-left">Form Pendaftaran Mahasiswa
		</div>
	</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
			</div>
			<div class="col-md-6">
				<a href="${pageContext.request.contextPath}/mahasiswa"><button type="button" class="btn btn-primary">Mahasiswa</button></a>
				<a href="${pageContext.request.contextPath}/jurusan"><button type="button" class="btn btn-primary">Jurusan</button></a>
				<a href="${pageContext.request.contextPath}/fakultas"><button type="button" class="btn btn-primary">Fakultas</button></a>
				<a href="${pageContext.request.contextPath}/matakuliah"><button type="button" class="btn btn-primary">Matakuliah</button></a>
				<hr />
				
				<form action="${pageContext.request.contextPath}/mahasiswa/store" method="post">
					<input hidden type="text" name="mode" value="${mode}">
					<br />
					<legend>Data Mahasiswa</legend>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Nama Lengkap<i class="text-danger">*</i></label> 
								<input type="text" name="nama" class="form-control" value="${mahasiswa.nama}" placeholder="Masukkan Nama">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">			
								<label>Tanggal Lahir</label>
								<input type="date" name="tanggallahir" class="form-control" value="${mahasiswa.tanggallahir}">
							</div>
						</div>			
						<div class="col-sm-6">
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<div class="row">
									<div class="col-sm-6">
										<input type="radio" name="jeniskelamin" value="Pria" <c:if test="${mahasiswa.jeniskelamin == 'Pria'}"> <c:out value="checked"/></c:if>>Pria
										<input type="radio" name="jeniskelamin" value="Wanita" <c:if test="${mahasiswa.jeniskelamin == 'Wanita'}"> <c:out value="checked"/></c:if>>Wanita
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" class="form-control" value="${mahasiswa.email}" placeholder="Masukkan Email">										
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Agama</label>
								<div class="row">
									<div class="col-sm-6">
									<select name="agama" class="form-control" >
										<option	<c:if test="${mahasiswa.agama == ''}"><c:out value="selected"/></c:if>>-Pilih Agama-</option>
										<option value="Islam" <c:if test="${mahasiswa.agama == 'Islam'}"> <c:out value="selected"/></c:if>>Islam</option>
										<option value="Kristen" <c:if test="${mahasiswa.agama == 'Kristen'}"><c:out value="selected"/></c:if>>Kristen</option>
										<option value="Katolik" <c:if test="${mahasiswa.agama == 'Katolik'}"><c:out value="selected"/></c:if>>Katolik</option>
										<option value="Hindu" <c:if test="${mahasiswa.agama == 'Hindu'}"><c:out value="selected"/></c:if>>Hindu</option>
											<option value="Budha" <c:if test="${mahasiswa.agama == 'Budha'}"><c:out value="selected"/></c:if>>Budha</option>
									</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Jurusan</label> &ensp; &ensp;
								<button type="button" class="btn btn-default" id="pilih-jurusan" data-toggle="modal" data-target="#jurusan-modal">Pilih Jurusan <span class="glyphicon glyphicon-chevron-right"></span></button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Alamat</label>
								<div class="row">
									<div class="col-sm-12">
									<textarea name="alamat" class="form-control" rows="3" cols="50" placeholder="Alamat">${mahasiswa.alamat} </textarea>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="row">
						<button type="submit" class="btn btn-success pull-right">
							<span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;&nbsp;Submit
						</button>
						<button type="reset" class="btn btn-danger">
							<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;&nbsp;Reset
						</button>
					</div>
					
					<!-- ---------------------------------------------- modall ----------------------------------------------  -->
					<div class="modal fade" tabindex="-1" role="dialog" id="jurusan-modal">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Pilih Jurusan</h4>
					      </div>
					       	<div class="modal-body" id="jurusan-modal-body">
        						<div id="field_1" class="col-sm-12">
						     		<div class="col-sm-9">
						     			<select name="jurusan[]" class="form-control" >
											<option	hidden value="<c:if test="${mahasiswa.agama == ''}"><c:out value="selected"/></c:if>" >-Pilih Jurusan-</option>
											<c:forEach var="jurusan" items="${ listJurusan }">
												<option value="${ jurusan.id }">${ jurusan.nama }</option>
											</c:forEach>
										</select>
						     		</div>
					     			<div class="col-sm-3 add-minus-button">
										<button type="button" class="btn btn-default add-jurusan-btn"><span class="glyphicon glyphicon-plus"></span></button>
										<button type="button" class="btn btn-default delete-jurusan-btn"><span class="glyphicon glyphicon-minus"></span></button>
									</div>
						     	</div><!-- end of field -->
						     	
      						</div><!-- end of modal-body -->
      						<br />
      						<br />
      						
					     	
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					        <button type="button" class="btn btn-primary">OK</button>
					      </div>
					    </div><!-- /.modal-content -->
					  </div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
					<!-- ---------------------------------------------- modall ----------------------------------------------  -->
					
				</form>
				<%-- <%= getServletContext().getAttribute("namaAttribute") %> --%>
			</div>
			<div class="col-md-3">
			</div>
		</div>
	</div>
	<script	src="${pageContext.request.contextPath}/resources/assets/js/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
	<script>
		var field_count = 1;
	
		$(document).on('click', '.add-jurusan-btn', function(){
			$('#field_'+field_count+' .add-minus-button').remove();
			
			$('#jurusan-modal-body').append(''+
				'<div id="field_'+ (++field_count) +'" class="col-sm-12" style="padding-top:5px;">'+
		     		'<div class="col-sm-9">'+
		     			'<select name="jurusan[]" class="form-control" >'+
							'<option	hidden value <c:if test="${mahasiswa.agama == ''}"><c:out value="selected"/></c:if> >-Pilih Agama-</option>'+
							'<option value="Islam" <c:if test="${mahasiswa.agama == 'Islam'}"> <c:out value="selected"/></c:if>>Islam</option>'+
							'<option value="Kristen" <c:if test="${mahasiswa.agama == 'Kristen'}"><c:out value="selected"/></c:if>>Kristen</option>'+
							'<option value="Katolik" <c:if test="${mahasiswa.agama == 'Katolik'}"><c:out value="selected"/></c:if>>Katolik</option>'+
							'<option value="Hindu" <c:if test="${mahasiswa.agama == 'Hindu'}"><c:out value="selected"/></c:if>>Hindu</option>'+
							'<option value="Budha" <c:if test="${mahasiswa.agama == 'Budha'}"><c:out value="selected"/></c:if>>Budha</option>'+
						'</select>'+
		     		'</div>'+
	     			'<div class="col-sm-3 add-minus-button">'+
						'<button type="button" class="btn btn-default add-jurusan-btn"><span class="glyphicon glyphicon-plus"></span></button>'+
						'<button type="button" class="btn btn-default delete-jurusan-btn"><span class="glyphicon glyphicon-minus"></span></button>'+
					'</div>'+
					'<!-- end of field -->'+
		     	'</div>'+
			'');
		});
		
		
		$(document).on('click', '.delete-jurusan-btn', function(){
			if(field_count>1) {
				$('#field_'+(field_count-1)).append(''+
					'<div class="col-sm-3 add-minus-button">'+
						'<button type="button" class="btn btn-default add-jurusan-btn"><span class="glyphicon glyphicon-plus"></span></button>'+
						'<button type="button" class="btn btn-default delete-jurusan-btn"><span class="glyphicon glyphicon-minus"></span></button>'+
					'</div>'+		
				'');
				
				$('#field_'+field_count--).remove();
			}
		});
	</script>
</body>
</html>