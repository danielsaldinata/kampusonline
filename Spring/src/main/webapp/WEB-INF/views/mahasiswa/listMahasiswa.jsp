<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<p class="navbar-text navbar-left">List Data Mahasiswa
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<a href="${pageContext.request.contextPath}/mahasiswa"><button type="button" class="btn btn-primary">Mahasiswa</button></a>
				<a href="${pageContext.request.contextPath}/jurusan"><button type="button" class="btn btn-primary">Jurusan</button></a>
				<a href="${pageContext.request.contextPath}/fakultas"><button type="button" class="btn btn-primary">Fakultas</button></a>
				<a href="${pageContext.request.contextPath}/matakuliah"><button type="button" class="btn btn-primary">Matakuliah</button></a>
				<hr />	
			
				<form action="${pageContext.request.contextPath }/mahasiswa/store2" method="post">
				<legend>Data Mahasiswa</legend>
				
				<a href="${pageContext.request.contextPath}/mahasiswa/create"><button type="button" class="btn btn-primary">Create</button></a>
				<br />
				<br />
				
				
				
				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">				
							<label>Nama</label>
						</div>
						<div class="col-sm-2">	
							<label>Alamat</label>
						</div>
						<div class="col-sm-2">	
							<label>Tanggal Lahir</label>
						</div>
						<div class="col-sm-2">	
							<label>Email</label>
						</div>
						<div class="col-sm-2">	
							<label>Jenis Kelamin</label>
						</div>
						<div class="col-sm-2">	
							<label>Agama</label>
						</div>
						
					</div>
					<c:forEach var="m" items="${listMahasiswa}">
					<div class="row">
						<div class="row">
							<div class="col-md-12">
								<div class="col-sm-2">	
									<input type="text" class="form-control" value="${m.nama}" name="nama">
								</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" value="${m.alamat}" name="alamat">
								</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" value="${m.tanggallahir}" name="tanggallahir">
								</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" value="${m.email}" name="email">
								</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" value="${m.jeniskelamin}" name="jeniskelamin">
								</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" value="${m.agama}" name="agama">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="col-sm-12" style="padding-top: 5px;]">
									<a href="${pageContext.request.contextPath}/mahasiswa/edit?nama=${m.nama}"><button type="button" class="btn btn-warning">Edit</button></a>
									<a href="${pageContext.request.contextPath}/mahasiswa/delete?nama=${m.nama}"><button type="button" class="btn btn-danger">Delete</button></a>	
								</div>
							</div>
						</div>
					</div>
					</form>
					</c:forEach>
				
				</div>
			
		</div>
	</div>
<script	src="${pageContext.request.contextPath}>/resources/assets/js/jquery.min.js"></script>
	<script src="<${pageContext.request.contextPath}>/resources/assets/js/bootstrap.min.js"></script>

</body>
</html>