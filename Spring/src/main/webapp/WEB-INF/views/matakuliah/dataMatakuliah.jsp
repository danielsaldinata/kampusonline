<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<p class="navbar-text navbar-left">Form Input Matakuliah
		</div>
	</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
			</div>
			<div class="col-md-6">
				<a href="${pageContext.request.contextPath}/mahasiswa"><button type="button" class="btn btn-primary">Mahasiswa</button></a>
				<a href="${pageContext.request.contextPath}/jurusan"><button type="button" class="btn btn-primary">Jurusan</button></a>
				<a href="${pageContext.request.contextPath}/fakultas"><button type="button" class="btn btn-primary">Fakultas</button></a>
				<a href="${pageContext.request.contextPath}/matakuliah"><button type="button" class="btn btn-primary">Matakuliah</button></a>
				<hr />	
				<form action="${pageContext.request.contextPath}/jurusan/store" method="post">
				
					<%-- <c:if test="${mode == 1}"> --%>
						<input type="hidden" name="mode" value="${mode}">
					<%-- </c:if> --%>
					<br />
					<legend>Data Matakuliah</legend>
					<input hidden type="text" name="id" value="${matakuliah.id}"/>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Nama Matakuliah<i class="text-danger">*</i></label> 
								<input type="text" name="nama" class="form-control" value="${matakuliah.nama}" placeholder="Masukkan Nama">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>SKS<i class="text-danger">*</i></label> 
								<input type="text" name="status" class="form-control" value="${matakuliah.sks}" placeholder="Masukkan Status">
							</div>
						</div>
					</div>
					
					<div class="row">
						<button type="submit" class="btn btn-success pull-right">
							<span class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></span>&nbsp;&nbsp;Submit
						</button>
						<button type="reset" class="btn btn-danger">
							<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;&nbsp;Reset
						</button>
					</div>
					
				</form>
			</div>
			<div class="col-md-3">
			</div>
		</div>
	</div>
	<script	src="${pageContext.request.contextPath}/resources/assets/js/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
	<script>
		
	</script>
</body>
</html>