package com.xsis.spring1.dao;

import java.util.Collection;

import com.xsis.spring1.model.Dosen;
import com.xsis.spring1.model.Mahasiswa;

public interface MahasiswaDao {
	public Collection<Mahasiswa> listMahasiswa() throws Exception;
	public void insertMahasiswa(Mahasiswa mahasiswa) throws Exception;
	public void updateMahasiswa(Mahasiswa mahasiswa) throws Exception;
	public void deleteMahasiswa(Mahasiswa mahasiswa) throws Exception;
	public Mahasiswa getByName(String nama)throws Exception;
}
