package com.xsis.spring1.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.spring1.dao.FakultasDao;
import com.xsis.spring1.model.Fakultas;

@Repository
public class FakultasDaoImpl implements FakultasDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Collection<Fakultas> listFakultas() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Fakultas";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public void insertFakultas(Fakultas fakultas) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(fakultas);
	}

	@Override
	public void updateFakultas(Fakultas fakultas) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.update(fakultas);
	}

	@Override
	public void deleteFakultas(Fakultas fakultas) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.delete(fakultas);
	}

	@Override
	public Fakultas getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Fakultas where id=:id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		
		return (Fakultas) query.list().get(0);
	}

	

}
