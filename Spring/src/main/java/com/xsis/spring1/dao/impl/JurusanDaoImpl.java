package com.xsis.spring1.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.spring1.dao.JurusanDao;
import com.xsis.spring1.model.Jurusan;

@Repository
public class JurusanDaoImpl implements JurusanDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Collection<Jurusan> listJurusan() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Jurusan";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public void insertJurusan(Jurusan jurusan) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(jurusan);
	}

	@Override
	public void updateJurusan(Jurusan jurusan) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.update(jurusan);
	}

	@Override
	public void deleteJurusan(Jurusan jurusan) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.delete(jurusan);
	}

	@Override
	public Jurusan getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Jurusan where id=:id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		
		return (Jurusan) query.list().get(0);
	}

}
