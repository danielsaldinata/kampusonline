package com.xsis.spring1.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
/*import org.springframework.orm.hibernate3.support.HibernateDaoSupport;*/
import org.springframework.stereotype.Repository;

import com.xsis.spring1.dao.MahasiswaDao;
import com.xsis.spring1.model.Mahasiswa;

@Repository
public class MahasiswaDaoImpl implements MahasiswaDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Mahasiswa> listMahasiswa() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Mahasiswa";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public void insertMahasiswa(Mahasiswa mahasiswa) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(mahasiswa);
		
	}

	@Override
	public void updateMahasiswa(Mahasiswa mahasiswa) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(mahasiswa);
	}

	@Override
	public void deleteMahasiswa(Mahasiswa mahasiswa) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(mahasiswa);
	}

	@Override
	public Mahasiswa getByName(String nama) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Mahasiswa where nama=:nama";
		Query query = session.createQuery(hql);
		query.setString("nama", nama);
		
		return (Mahasiswa) query.list().get(0);
	}

}
