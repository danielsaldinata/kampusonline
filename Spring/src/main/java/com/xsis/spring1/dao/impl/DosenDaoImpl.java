package com.xsis.spring1.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.spring1.model.Dosen;
import com.xsis.spring1.service.DosenService;

@Repository
public class DosenDaoImpl implements DosenService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Dosen> listDosen() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Dosen";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public void insertDosen(Dosen dosen) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(dosen);
	}

	@Override
	public void updateDosen(Dosen dosen) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(dosen);
	}

	@Override
	public void deleteDosen(Dosen dosen) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(dosen);
	}

	@Override
	public Dosen getByName(String nama) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Dosen where nama=:nama";
		Query query = session.createQuery(hql);
		query.setString("nama", nama);
		
		return (Dosen) query.list().get(0);
	}

}
