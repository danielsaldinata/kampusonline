package com.xsis.spring1.dao;

import java.util.Collection;

import com.xsis.spring1.model.Dosen;

public interface DosenDao {
	public Collection<Dosen> listDosen()throws Exception;
	public void insertDosen(Dosen dosen)throws Exception;
	public void updateDosen(Dosen dosen)throws Exception;
	public void deleteDosen(Dosen dosen)throws Exception;
	public Dosen getByName(String nama) throws Exception;
}
