package com.xsis.spring1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_DOSEN")
public class Dosen {
	private String nama;
	private String tanggallahir;
	private String jeniskelamin;
	private String email;
	private String agama;
	private String alamat;
	/**
	 * @return the nama
	 */
	@Id
	@Column(name="dsn_nama")
	public String getNama() {
		return nama;
	}
	/**
	 * @param nama the nama to set
	 */
	public void setNama(String nama) {
		this.nama = nama;
	}
	/**
	 * @return the tanggallahir
	 */
	@Column(name="dsn_tgllahir")
	public String getTanggallahir() {
		return tanggallahir;
	}
	/**
	 * @param tanggallahir the tanggallahir to set
	 */
	public void setTanggallahir(String tanggallahir) {
		this.tanggallahir = tanggallahir;
	}
	/**
	 * @return the jeniskelamin
	 */
	@Column(name="dsn_gender")
	public String getJeniskelamin() {
		return jeniskelamin;
	}
	/**
	 * @param jeniskelamin the jeniskelamin to set
	 */
	public void setJeniskelamin(String jeniskelamin) {
		this.jeniskelamin = jeniskelamin;
	}
	/**
	 * @return the email
	 */
	@Column(name="dsn_email")
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the agama
	 */
	@Column(name="dsn_agama")
	public String getAgama() {
		return agama;
	}
	/**
	 * @param agama the agama to set
	 */
	public void setAgama(String agama) {
		this.agama = agama;
	}
	/**
	 * @return the alamat
	 */
	@Column(name="dsn_alamat")
	public String getAlamat() {
		return alamat;
	}
	/**
	 * @param alamat the alamat to set
	 */
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Dosen [nama=" + nama + ", tanggallahir=" + tanggallahir + ", jeniskelamin=" + jeniskelamin + ", email="
				+ email + ", agama=" + agama + ", alamat=" + alamat + "]";
	}
	

}
