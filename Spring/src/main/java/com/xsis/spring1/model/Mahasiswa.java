package com.xsis.spring1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
// ini comment daniel
@Entity
@Table(name="T_MAHASISWA")
public class Mahasiswa {
	private String nama;
	private String tanggallahir;
	private String alamat;
	private String jeniskelamin;
	private String email;
	private String agama;
	
	@Id
	@Column(name="mhs_nama")
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	@Column(name="mhs_tgllahir")
	public String getTanggallahir() {
		return tanggallahir;
	}
	public void setTanggallahir(String tanggallahir) {
		this.tanggallahir = tanggallahir;
	}
	
	@Column(name="mhs_alamat")
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	@Column(name="mhs_gender")
	public String getJeniskelamin() {
		return jeniskelamin;
	}
	public void setJeniskelamin(String jeniskelamin) {
		this.jeniskelamin = jeniskelamin;
	}
	
	@Column(name="mhs_email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="mhs_agama")
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	
	@Override
	public String toString() {
		return "Mahasiswa [nama=" + nama + ", tanggallahir=" + tanggallahir + ", alamat=" + alamat + ", jeniskelamin="
				+ jeniskelamin + ", email=" + email + ", agama=" + agama + "]";
	}
	
	
	
	
}
