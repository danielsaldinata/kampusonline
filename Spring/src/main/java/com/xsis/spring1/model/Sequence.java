package com.xsis.spring1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "T_SEQUENCE")
public class Sequence {
	private String id;
	private Integer nomor;
	
	@Id
	@Column(name="SEQ_NAME")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getNomor() {
		return nomor;
	}
	public void setNomor(Integer nomor) {
		this.nomor = nomor;
	}
	
	
}
