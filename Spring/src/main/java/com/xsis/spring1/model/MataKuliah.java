package com.xsis.spring1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_MATAKULIAH")
public class MataKuliah {
	
	@Id
	@GeneratedValue
	@Column(name="mk_id")
	private int id;
	
	@Column(name="mk_nama")
	private String nama;
	
	@Column(name="mk_sks")
	private int sks;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getSks() {
		return sks;
	}

	public void setSks(int sks) {
		this.sks = sks;
	}

	@Override
	public String toString() {
		return "MataKuliah [id=" + id + ", nama=" + nama + ", sks=" + sks + "]";
	}
	
	
	
	
}
