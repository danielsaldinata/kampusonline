package com.xsis.spring1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_FAKULTAS")
public class Fakultas {

	@Id
	@GeneratedValue
	@Column(name="fa_id")
	private int id ;
	
	@Column(name="fa_nama")
	private String nama;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Override
	public String toString() {
		return "Fakultas [id=" + id + ", nama=" + nama + "]";
	}
	
	
	
	
	
}
