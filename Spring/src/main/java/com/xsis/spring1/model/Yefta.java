package com.xsis.spring1.model;

public class Yefta {

	private String nama="Yefta Elia Laoh";
	private String alamat="Bandung";
	/**
	 * @return the name
	 */
	public String getNama() {
		return nama;
	}
	/**
	 * @param name the name to set
	 */
	public void setNama(String name) {
		this.nama = name;
	}
	/**
	 * @return the alamat
	 */
	public String getAlamat() {
		return alamat;
	}
	/**
	 * @param alamat the alamat to set
	 */
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	
}
