package com.xsis.spring1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="T_JURUSAN")
public class Jurusan {

	@Id
	@GeneratedValue
	@Column(name="jrsn_id")
	private int id;
	
	@Column(name="jrsn_nama")
	private String nama;
	
	@Column(name="jrsn_status")
	private String status;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Jurusan [kode=" + id + ", nama=" + nama + ", status=" + status + "]";
	}
	
	
	
}
