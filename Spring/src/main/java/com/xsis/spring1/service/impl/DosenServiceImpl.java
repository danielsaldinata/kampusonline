package com.xsis.spring1.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.spring1.dao.DosenDao;
import com.xsis.spring1.model.Dosen;
import com.xsis.spring1.service.DosenService;

@Service
@Transactional
public class DosenServiceImpl implements DosenService {
	
	@Autowired
	private DosenDao dosenDao;
	
	@Override
	public Collection<Dosen> listDosen() throws Exception {
		// TODO Auto-generated method stub
		return dosenDao.listDosen();
	}

	@Override
	public void insertDosen(Dosen dosen) throws Exception {
		// TODO Auto-generated method stub
		dosenDao.insertDosen(dosen);
	}

	@Override
	public void updateDosen(Dosen dosen) throws Exception {
		// TODO Auto-generated method stub
		dosenDao.updateDosen(dosen);
	}

	@Override
	public void deleteDosen(Dosen dosen) throws Exception {
		// TODO Auto-generated method stub
		dosenDao.deleteDosen(dosen);
	}

	@Override
	public Dosen getByName(String nama) throws Exception {
		// TODO Auto-generated method stub
		return dosenDao.getByName(nama);
	}

}
