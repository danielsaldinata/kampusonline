package com.xsis.spring1.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.spring1.dao.MahasiswaDao;
import com.xsis.spring1.model.Mahasiswa;
import com.xsis.spring1.service.MahasiswaService;

@Service
@Transactional
public class MahasiswaServiceImpl implements MahasiswaService{
	@Autowired
	private MahasiswaDao mahasiswaDao;
	@Override
	public Collection<Mahasiswa> listMahasiswa() throws Exception {
		// TODO Auto-generated method stub
		return mahasiswaDao.listMahasiswa();
	}

	@Override
	public void insertMahasiswa(Mahasiswa mahasiswa) throws Exception {
		// TODO Auto-generated method stub
		mahasiswaDao.insertMahasiswa(mahasiswa);
	}

	@Override
	public void updateMahasiswa(Mahasiswa mahasiswa) throws Exception {
		// TODO Auto-generated method stub
		mahasiswaDao.updateMahasiswa(mahasiswa);
	}

	@Override
	public void deleteMahasiswa(Mahasiswa mahasiswa) throws Exception {
		// TODO Auto-generated method stub
		mahasiswaDao.deleteMahasiswa(mahasiswa);
	}

	@Override
	public Mahasiswa getByName(String nama) throws Exception {
		// TODO Auto-generated method stub
		return mahasiswaDao.getByName(nama);
	}
	
	
	
}
