package com.xsis.spring1.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.spring1.dao.FakultasDao;
import com.xsis.spring1.model.Fakultas;
import com.xsis.spring1.service.FakultasService;

@Service
@Transactional
public class FakultasServiceImpl implements FakultasService{

	@Autowired
	private FakultasDao fakultasDao;
	
	@Override
	public Collection<Fakultas> listFakultas() throws Exception {
		return fakultasDao.listFakultas();
	}

	@Override
	public void insertFakultas(Fakultas fakultas) throws Exception {
		fakultasDao.insertFakultas(fakultas);
	}

	@Override
	public void updateFakultas(Fakultas fakultas) throws Exception {
		fakultasDao.updateFakultas(fakultas);
	}

	@Override
	public void deleteFakultas(Fakultas fakultas) throws Exception {
		fakultasDao.deleteFakultas(fakultas);
	}

	@Override
	public Fakultas getById(int id) throws Exception {
		return fakultasDao.getById(id);
	}

}
