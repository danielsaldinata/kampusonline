package com.xsis.spring1.service;

import java.util.Collection;

import com.xsis.spring1.model.Jurusan;

public interface JurusanService {
	public Collection<Jurusan> listJurusan() throws Exception;
	public void insertJurusan(Jurusan jurusan) throws Exception;
	public void updateJurusan(Jurusan jurusan) throws Exception;
	public void deleteJurusan(Jurusan jurusan) throws Exception;
	public Jurusan getById(int id) throws Exception;

}
