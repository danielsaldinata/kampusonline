package com.xsis.spring1.service;

import java.util.Collection;

import org.springframework.stereotype.Controller;
import com.xsis.spring1.model.Dosen;
import com.xsis.spring1.model.Mahasiswa;

public interface DosenService {
	public Collection<Dosen> listDosen()throws Exception;
	public void insertDosen(Dosen dosen)throws Exception;
	public void updateDosen(Dosen dosen)throws Exception;
	public void deleteDosen(Dosen dosen)throws Exception;
	public Dosen getByName(String nama) throws Exception;

}
