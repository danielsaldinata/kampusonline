package com.xsis.spring1.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.spring1.model.Jurusan;
import com.xsis.spring1.service.JurusanService;

@Controller
public class JurusanController {
	
	@Autowired
	private JurusanService jurusanService;
	private Collection<Jurusan> listJurusan;

	@RequestMapping(value="/jurusan", method=RequestMethod.GET)
	public String index(Model model) {

		try {
			listJurusan = jurusanService.listJurusan();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		model.addAttribute("listJurusan", listJurusan);
		
		return "jurusan/index";
	}
	
	@RequestMapping(value="/jurusan/create", method=RequestMethod.GET)
	public String create(Model model) {
		
		model.addAttribute("mode",0 );
		model.addAttribute("jurusanId", 0);
		return "jurusan/create";
	}
	
	@RequestMapping(value="/jurusan/store", method=RequestMethod.POST)
	public String store (Model model, @ModelAttribute Jurusan jurusan, HttpServletRequest request) {
		
		String mode="0";
		
		if(request.getParameter("mode")!=null) 
			mode = request.getParameter("mode");
			
		try {
			if(mode.equalsIgnoreCase("0"))
				jurusanService.insertJurusan(jurusan);
			else 
				jurusanService.updateJurusan(jurusan);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/jurusan";
	}
	
	@RequestMapping(value="/jurusan/edit", method=RequestMethod.GET)
	public String edit(Model model, Jurusan jurusan) {
		Jurusan editedJurusan = new Jurusan();
		
		try {
			editedJurusan = jurusanService.getById(jurusan.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("jurusan", editedJurusan);
		model.addAttribute("mode", "1");
		model.addAttribute("jurusanId", editedJurusan.getId());
	
		
		return "jurusan/create";
	}
	
	
	@RequestMapping(value="/jurusan/delete", method=RequestMethod.GET)
	public String delete(Jurusan jurusan) {
		
		try {
			jurusanService.deleteJurusan(jurusan);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/jurusan";
	}

}
