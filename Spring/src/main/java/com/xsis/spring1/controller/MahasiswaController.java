package com.xsis.spring1.controller;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.spring1.model.Jurusan;
import com.xsis.spring1.model.Mahasiswa;
import com.xsis.spring1.service.JurusanService;
import com.xsis.spring1.service.MahasiswaService;

@Controller
public class MahasiswaController {
	@Autowired
	private MahasiswaService mahasiswaService;
	@Autowired
	private JurusanService jurusanService;
	
	private Collection<Mahasiswa> listMahasiswa;
	
	public MahasiswaController () {
		this.listMahasiswa = new ArrayList<Mahasiswa>();
	}
	
	/*private static final Logger logger = LoggerFactory.getLogger(MahasiswaController.class);
	*/
	@RequestMapping(value = "/mahasiswa/create", method = RequestMethod.GET)
	public String mahasiswa(Model model) {
		
		Collection<Jurusan> listJurusan = new ArrayList<Jurusan>();
		try {
			listJurusan = jurusanService.listJurusan();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		model.addAttribute("listJurusan", listJurusan);
		model.addAttribute("mode",0 );
		
		return "mahasiswa/dataMahasiswa";
	}
	
	@RequestMapping(value = "/mahasiswa", method = RequestMethod.GET)
	public String listMahasiswa(Model model) {
		
		try {
			listMahasiswa= mahasiswaService.listMahasiswa();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("listMahasiswa", listMahasiswa);
		
		return "mahasiswa/listMahasiswa";
	}
	
	
	@RequestMapping(value = "/mahasiswa/store", method = RequestMethod.POST)
	public String store(Model model, Mahasiswa mahasiswa, HttpServletRequest request) {
		
		String mode="0";
		if(request.getParameter("mode")!=null){
			mode=request.getParameter("mode");
		}
		try {			
			 if(mode.equalsIgnoreCase("0")){
				 mahasiswaService.insertMahasiswa(mahasiswa);
				 listMahasiswa= mahasiswaService.listMahasiswa();
			 }
			 else{ 
				 mahasiswaService.updateMahasiswa(mahasiswa);
				 listMahasiswa= mahasiswaService.listMahasiswa();
			 }
				 //
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		model.addAttribute("listMahasiswa", listMahasiswa);
		
		return "redirect:/mahasiswa";
	}
	
	@RequestMapping(value = "/mahasiswa/edit", method = RequestMethod.GET)
	public String store2(Model model, Mahasiswa mahasiswa) {
		Mahasiswa m=new Mahasiswa();
		
		try {
			m= mahasiswaService.getByName(mahasiswa.getNama());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("mahasiswa", m);
		model.addAttribute("mode", "1");
		
		return "mahasiswa/dataMahasiswa";
	}
	@RequestMapping(value = "/mahasiswa/delete", method = RequestMethod.GET)
	public String deleteMahasiswa(Model model, Mahasiswa mahasiswa) {
		Mahasiswa m=new Mahasiswa();
		try {
			m= mahasiswaService.getByName(mahasiswa.getNama());
			mahasiswaService.deleteMahasiswa(m);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:/mahasiswa";
	}
	
	
	
	
	
	
}
