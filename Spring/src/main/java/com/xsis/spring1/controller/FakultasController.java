package com.xsis.spring1.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.spring1.model.Fakultas;
import com.xsis.spring1.service.FakultasService;
//tes
@Controller
public class FakultasController {
	
	@Autowired
	private FakultasService fakultasService;
	private Collection<Fakultas> listFakultas;

	@RequestMapping(value="/fakultas", method=RequestMethod.GET)
	public String index(Model model) {

		try {
			listFakultas = fakultasService.listFakultas();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		model.addAttribute("listFakultas", listFakultas);
		
		return "fakultas/index";
	}
	
	@RequestMapping(value="/fakultas/create", method=RequestMethod.GET)
	public String create(Model model) {
		
		model.addAttribute("mode",0 );
		model.addAttribute("fakultasId", 0);
		return "fakultas/create";
	}
	
	@RequestMapping(value="/fakultas/store", method=RequestMethod.POST)
	public String store (Model model, @ModelAttribute Fakultas fakultas, HttpServletRequest request) {
		
		String mode="0";
		
		if(request.getParameter("mode")!=null) 
			mode = request.getParameter("mode");
			
		try {
			if(mode.equalsIgnoreCase("0"))
				fakultasService.insertFakultas(fakultas);
			else 
				fakultasService.updateFakultas(fakultas);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/fakultas";
	}
	
	@RequestMapping(value="/fakultas/edit", method=RequestMethod.GET)
	public String edit(Model model, Fakultas fakultas) {
		Fakultas editedFakultas = new Fakultas();
		
		try {
			editedFakultas = fakultasService.getById(fakultas.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("fakultas", editedFakultas);
		model.addAttribute("mode", "1");
		model.addAttribute("fakultasId", editedFakultas.getId());
	
		
		return "fakultas/create";
	}
	
	
	@RequestMapping(value="/fakultas/delete", method=RequestMethod.GET)
	public String delete(Fakultas fakultas) {
		
		try {
			fakultasService.deleteFakultas(fakultas);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "redirect:/fakultas";
	}

}
